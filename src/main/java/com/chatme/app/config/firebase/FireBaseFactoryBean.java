package com.chatme.app.config.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseOptions;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StringUtils;

import java.util.List;

public class FireBaseFactoryBean extends AbstractFactoryBean<FireBaseBean> {
    private static final ResourceLoader RESOURCE_LOADER = new DefaultResourceLoader();
    private String jsonPath;
    private String dataUrl;
    private List<String> appNames;
    private ResourceLoader resourceLoader = RESOURCE_LOADER;

    @Override
    public Class<?> getObjectType() {
        return FireBaseBean.class;
    }

    @NotNull
    @Override
    protected FireBaseBean createInstance() throws Exception {
        FirebaseSetting firebaseSetting = FirebaseSetting
                .builder()
                .jsonPath(jsonPath)
                .appNames(appNames)
                .build();
        Resource resource = resourceLoader.getResource(jsonPath);
        GoogleCredentials googleCredentials = GoogleCredentials.fromStream(resource.getInputStream());
        FirebaseOptions.Builder builder = FirebaseOptions.builder().setCredentials(googleCredentials);
        if(StringUtils.hasText(dataUrl)) {
            builder.setDatabaseUrl(dataUrl);
        }
        FireBaseBean fireBaseBean = new FireBaseBean(builder.build(), firebaseSetting);
        fireBaseBean.init();
        return fireBaseBean;
    }


    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader == null ? RESOURCE_LOADER : resourceLoader;
    }

    public void setAppNames(List<String> appNames) {
        this.appNames = appNames;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public void setJsonPath(String jsonPath) {
        this.jsonPath = jsonPath;
    }
}

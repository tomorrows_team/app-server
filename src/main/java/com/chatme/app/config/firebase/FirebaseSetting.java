package com.chatme.app.config.firebase;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FirebaseSetting {
    private String jsonPath;
    private List<String> appNames;
}

package com.chatme.app.config.firebase;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class FireBaseBean {
    private final FirebaseOptions firebaseOptions;
    private final FirebaseSetting firebaseSetting;
    private static final Map<String,FirebaseAuth> firebaseAuthMap = new ConcurrentHashMap<>();

    public FireBaseBean(FirebaseOptions firebaseOptions,FirebaseSetting firebaseSetting) {
        this.firebaseOptions = firebaseOptions;
        this.firebaseSetting = firebaseSetting;
    }

    void init() {
        List<String> appNames = firebaseSetting.getAppNames();
        if(CollectionUtils.isEmpty(appNames)){
            FirebaseApp instance;
            try {
                instance = FirebaseApp.getInstance();
                if (Objects.isNull(instance)) {
                    instance = FirebaseApp.initializeApp(firebaseOptions);
                }
            } catch(Exception e){
                instance = FirebaseApp.initializeApp(firebaseOptions);
            }
            firebaseAuthMap.put(instance.getName(),FirebaseAuth.getInstance(instance));
        } else {
            FirebaseApp instance;
            for (String appName : appNames) {
                try {
                    instance = FirebaseApp.getInstance(appName);
                    if (Objects.isNull(instance)) {
                        instance = FirebaseApp.initializeApp(firebaseOptions,appName);
                    }
                } catch(Exception e){
                    instance = FirebaseApp.initializeApp(firebaseOptions,appName);
                }
                firebaseAuthMap.put(instance.getName(),FirebaseAuth.getInstance(instance));
            }
        }
    }


    public FirebaseAuth firebaseAuth(String appName) {
        return firebaseAuthMap.get(appName);
    }

    public FirebaseAuth defaultFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }


    public boolean verifyUser(String appName,String uid) {
        FirebaseAuth firebaseAuth = StringUtils.hasText(appName)?firebaseAuth(appName):defaultFirebaseAuth();
        try {
            UserRecord userRecord = firebaseAuth.getUser(uid);
            return Objects.nonNull(userRecord);
        } catch (FirebaseAuthException exception) {
            log.error("error:{}",exception.getMessage());
            return false;
        }
    }

}

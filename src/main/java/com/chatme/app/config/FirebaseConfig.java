package com.chatme.app.config;

import com.chatme.app.config.firebase.FireBaseFactoryBean;
import com.chatme.app.config.firebase.FirebaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = FirebaseProperties.class)
public class FirebaseConfig {

    @Bean
    public FireBaseFactoryBean fireBaseFactoryBean() {
        FireBaseFactoryBean fireBaseFactoryBean = new FireBaseFactoryBean();
        fireBaseFactoryBean.setJsonPath("firebase/android-demo-419010-firebase-adminsdk-7x80h-4a5d4f0859.json");
        fireBaseFactoryBean.setDataUrl("https://chat_me.firebaseio.com/");
        return fireBaseFactoryBean;
    }
}

package com.chatme.app.config;

import com.chatme.app.config.factorys.LoginServiceFactory;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanFactoryConfig {

    @Bean
    public ServiceLocatorFactoryBean loginServiceFactoryBean() {
        ServiceLocatorFactoryBean ruleFactory = new ServiceLocatorFactoryBean();
        ruleFactory.setServiceLocatorInterface(LoginServiceFactory.class);
        return ruleFactory;
    }
}

package com.chatme.app.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix="sms")
@PropertySource(value = "classpath:config/tencent_sms.properties", encoding = "UTF-8")
public class TencentSMSConfig {
    public String secretId;
    public String secretKey;
    public String appId;
    public String templateId;
    public String sign;

}

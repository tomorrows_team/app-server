package com.chatme.app.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix="im")
@PropertySource(value = "classpath:config/im.properties", encoding = "UTF-8")
public class IMConfig {
    private String admin_url;
    private String admin_secret;
    private String admin_user_id;
    private boolean use_random_name;
    private String welcome_for_new_user;
    private String welcome_for_back_user;

    private boolean new_user_robot_friend;
    private String robot_friend_id;
    private String robot_welcome;

    private String prompt_text;
    private String image_msg_url;
    private String image_msg_base64_thumbnail;

    private String new_user_subscribe_channel_id;
    private String back_user_subscribe_channel_id;

}

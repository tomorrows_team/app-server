package com.chatme.app.config.factorys;

import com.chatme.app.service.LoginService;

public interface LoginServiceFactory {
    LoginService getLoginService(String name);
}

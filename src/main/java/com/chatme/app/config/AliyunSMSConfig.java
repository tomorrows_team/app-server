package com.chatme.app.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix="alisms")
@PropertySource(value = "classpath:config/aliyun_sms.properties",encoding = "utf-8")
public class AliyunSMSConfig {
    String accessKeyId;
    String accessSecret;
    String signName;
    String templateCode;

}

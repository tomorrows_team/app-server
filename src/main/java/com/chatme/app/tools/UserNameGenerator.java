package com.chatme.app.tools;

public interface UserNameGenerator {
    String getUserName(String phone);
}

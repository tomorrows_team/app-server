package com.chatme.app.pojo.login;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {
    /**
     * firebase uid
     */
    private String uid;
    /**
     * 邮箱地址
     */
    private String email;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 验证码
     */
    private String code;
    /**
     * 登录类型
     * @see com.chatme.app.common.enums.LoginModeEnum
     */
    private Integer loginMode;
    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 平台
     */
    private int platform;
}

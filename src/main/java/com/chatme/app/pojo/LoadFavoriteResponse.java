package com.chatme.app.pojo;

import com.chatme.app.jpa.FavoriteItem;

import java.util.List;

public class LoadFavoriteResponse {
    public List<FavoriteItem> items;
    public boolean hasMore;
}

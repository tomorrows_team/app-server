package com.chatme.app.jpa;

import org.springframework.data.repository.CrudRepository;

public interface ShiroSessionRepository extends CrudRepository<ShiroSession, String> {
}

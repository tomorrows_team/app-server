package com.chatme.app.common.enums;

import lombok.Getter;

import java.util.Objects;

@Getter
public enum LoginModeEnum {
    ANONYMOUS(1,"Anonymous"),
    EMAIL(2,"Email"),
    PHONE(3,"Phone"),
    ;
    private final Integer model;

    private final String clazz;

    LoginModeEnum(Integer model,String clazz) {
        this.model = model;
        this.clazz = clazz;
    }


    public static String ofName(Integer model) {
        for (LoginModeEnum modeEnum : values()) {
            if(Objects.deepEquals(modeEnum.model,model)){
                return modeEnum.clazz;
            }
        }
        throw new IllegalArgumentException();
    }
}

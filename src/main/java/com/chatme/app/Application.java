package com.chatme.app;

import cn.hutool.json.JSONUtil;
import com.chatme.app.config.firebase.FireBaseBean;
import com.chatme.app.jpa.PCSessionRepository;
import com.aliyuncs.ram.model.v20150501.CreateUserRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.multitenancy.Tenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@ServletComponentScan
@EnableScheduling
public class Application {
    @Autowired
    private PCSessionRepository pcSessionRepository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}


	/**
	 * 文件上传配置
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		//单个文件最大
		factory.setMaxFileSize(DataSize.ofMegabytes(20)); //20MB
		/// 设置总上传数据总大小
		factory.setMaxRequestSize(DataSize.ofMegabytes(100));
		return factory.createMultipartConfig();
	}

    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void clearPCSession(){
        pcSessionRepository.deleteByCreateDtBefore(System.currentTimeMillis() - 60 * 60 * 1000);
    }


	@Component
	static class FirebaseRunner implements ApplicationRunner {
		@Autowired
		private FireBaseBean fireBaseBean;
		@Override
		public void run(ApplicationArguments args) throws Exception {
			FirebaseAuth firebaseAuth = fireBaseBean.defaultFirebaseAuth();
			boolean smxn = fireBaseBean.verifyUser(null, "smxn");
			System.out.println(smxn);
		}
	}

}

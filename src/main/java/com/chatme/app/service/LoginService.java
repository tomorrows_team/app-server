package com.chatme.app.service;

import com.chatme.app.common.RestResult;
import com.chatme.app.pojo.login.LoginRequest;

import javax.servlet.http.HttpServletResponse;

public interface LoginService {
    RestResult login(HttpServletResponse response,LoginRequest request);
}

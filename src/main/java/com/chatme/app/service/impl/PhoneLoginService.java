package com.chatme.app.service.impl;

import com.chatme.app.common.RestResult;
import com.chatme.app.pojo.login.LoginRequest;
import com.chatme.app.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;


@Service("Phone")
public class PhoneLoginService implements LoginService {
    @Autowired
    private com.chatme.app.service.Service service;
    @Override
    public RestResult login(HttpServletResponse response,LoginRequest request) {
        return service.loginWithMobileCode(response, request.getPhone(), request.getCode(),request.getClientId(),request.getPlatform());
    }
}

package com.chatme.app.service.impl;

import com.chatme.app.common.RestResult;
import com.chatme.app.pojo.login.LoginRequest;
import com.chatme.app.service.LoginService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

@Service("Anonymous")
public class AnonymousLoginService implements LoginService {
    @Override
    public RestResult login(HttpServletResponse response,LoginRequest request) {

        return null;
    }
}

package com.chatme.app.service.conference;


import com.chatme.app.common.RestResult;
import com.chatme.app.pojo.*;

public interface ConferenceService {
    RestResult getUserConferenceId(String userId);
    RestResult getMyConferenceId();
    RestResult getConferenceInfo(String conferenceId, String password);
    RestResult putConferenceInfo(ConferenceInfo info);
    RestResult createConference(ConferenceInfo info);
    RestResult destroyConference(String conferenceId);
    RestResult recordingConference(String conferenceId, boolean recording);
    RestResult focusConference(String conferenceId, String userId);
    RestResult favConference(String conferenceId);
    RestResult unfavConference(String conferenceId);
    RestResult getFavConferences();
    RestResult isFavConference(String conferenceId);
}

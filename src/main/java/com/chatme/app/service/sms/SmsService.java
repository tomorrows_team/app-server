package com.chatme.app.service.sms;


import com.chatme.app.common.RestResult;

public interface SmsService {
    RestResult.RestCode sendCode(String mobile, String code);
}
